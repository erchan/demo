import { Component, OnInit } from '@angular/core';
import { Employee, AddFormSubmit } from '../data/employeedata';
import { Router } from '@angular/router';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  employees: Employee[] = [];
  Keyword: string = '';
  AddFormSubmit: AddFormSubmit = new AddFormSubmit();
  EditFS: Employee = new Employee();

  constructor(private _employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
    this.GET();
  }

  public GET() {
    this._employeeService.post<any>('/api/Employee/Get', this.Keyword).subscribe(
      (data: Employee[]) => {
        this.employees = data;
      });
  }

  public Del(id) {
    if (confirm('是否刪除資料')) {
      this._employeeService.post<any>('/api/Employee/Del', id).subscribe(
        (data: Employee[]) => {
          this.employees = data;
        });
    }
  }

  public Edit(id) {
    this.router.navigate(['/edit/' + id]);
  }

  public Add(id) {
    this.router.navigate(['/add']);
  }
}
