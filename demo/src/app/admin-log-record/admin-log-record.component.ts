import { Component, OnInit } from '@angular/core';
import { AdminLogDate } from '../data/logindata';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-log-record',
  templateUrl: './admin-log-record.component.html',
  styleUrls: ['./admin-log-record.component.css']
})
export class AdminLogRecordComponent implements OnInit {

  AdminLogDate: AdminLogDate[] =  [];

  constructor(private _employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
    this.LogGet();
  }

  public LogGet() {
    this._employeeService.post<any>('/api/Login/LogGet', null).subscribe(
      (data: AdminLogDate[]) => {
        this.AdminLogDate = data;
      });
  }
}
