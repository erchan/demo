export class LoginDate {
  ID: number;
  Account: string;
  Password: string;
  IsVerifyCode: boolean;
}

export class AdminLogDate {
  Id: number;
  AdminId: number;
  CreatedAt: string;
  IsSuccess: boolean;
}
