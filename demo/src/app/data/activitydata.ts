export class ActivityInfo {
  Id: number;
  User: UserInfo;
  CreatedAt: string;
  Activity: ActivityItem;
}

export class UserInfo {
  Id: number;
  LastName: string;
  FirstName: string;
}

export class ActivityItem {
  Id: number;
  Name: string;
}

export class ActivityUser {
  UserId: number;
  ActivityId: number;
  Email: string;
  CreatedAt: string;
}

