import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginDate } from '../data/logindata';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Injectable()
export class EmployeeService {
  API_URL = 'http://localhost:56778';
  _token = '';
  private _sessionId: string;
  LoginDate = new LoginDate();
  IsLogin: boolean = false;
  appcomponent: AppComponent;

  constructor(private httpClient: HttpClient, private router: Router, private cookieService: CookieService) { }

  createAuthorizationHeader(headers: HttpHeaders) {
    if (this._token) {
      headers.append('Authorization', 'Bearer ' + this._token);
    }
  }

  post<T>(path, data) {
    const fullPath = `${this.API_URL}${path}`;
    const headers = new HttpHeaders();
    this.createAuthorizationHeader(headers);
    return this.httpClient.post(fullPath, data, { headers: { 'Content-Type': 'application/json' } });
  }

  Adminlogin(LoginDate: LoginDate) {

    if (!!this.cookieService.get('admin')) {
      this.islogin();
    } else {
      this.LoginDate = LoginDate;
    }

    this.post<any>('/api/Login/Login', this.LoginDate).subscribe(
      (data: LoginDate) => {
        if (!!data.Account && !!data.Password) {
          this.LoginDate = data;
          this.sessionId(this.LoginDate.ID + '&' + this.LoginDate.Account + '&' + this.LoginDate.Password);

          if (window.location.href !== location.origin + '/list') {
            window.location.href = location.origin + '/list';
          }
        }
      });

    return this.LoginDate;
  }

  sessionId(value: string) {
    this._sessionId = value;
    this.cookieService.set('admin', value);
  }

  logout() {
    this.cookieService.delete('admin');
    this.router.navigate(['/login']);
  }

  islogin() {
    const cookieService = this.cookieService.get('admin').split('&');
    this.LoginDate.ID = +cookieService[0];
    this.LoginDate.Account = cookieService[1];
    this.LoginDate.Password = cookieService[2];
    return this.LoginDate;
  }
}
