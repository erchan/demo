import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from './service/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  IsLogin: boolean = false;

  constructor(private _employeeService: EmployeeService, private router: Router) {
  }

  ngOnInit() {
    this.IsLogin = !!this._employeeService.islogin().ID;
  }

  logout() {
    this._employeeService.logout();
  }
}
