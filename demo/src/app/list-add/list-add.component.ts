import { Component, OnInit } from '@angular/core';
import { Employee, AddFormSubmit } from '../data/employeedata';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-add',
  templateUrl: './list-add.component.html',
  styleUrls: ['./list-add.component.css']
})
export class ListAddComponent implements OnInit {

  employees: Employee[] = [];
  Keyword: string = '';
  AddFormSubmit: AddFormSubmit = new AddFormSubmit();
  EditFS: Employee = new Employee();

  constructor(private _employeeService: EmployeeService, private router: Router) { }

  ngOnInit() {
  }

  public Add() {
    if (confirm('是否新增資料')) {
      this._employeeService.post<any>('/api/Employee/Add', this.AddFormSubmit).subscribe(
        (data: Employee[]) => {
          this.employees = data;
          this.router.navigate(['/list']);
        });
    }
  }
}
