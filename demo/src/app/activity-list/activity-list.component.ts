import { Component, OnInit } from '@angular/core';
import { ActivityInfo } from '../data/activitydata';
import { ActivatedRoute, Router } from '@angular/router';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {

  ActivityList: ActivityInfo[] = [];

  constructor(private _employeeService: EmployeeService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this._employeeService.post<any>('/api/Activity/Get', '').subscribe(
      (data: ActivityInfo[]) => {
        this.ActivityList = data;
        console.log(this.ActivityList );
      });
  }

  public Add(id) {
    this.router.navigate(['/activityadd']);
  }
}
