import { Component, OnInit } from '@angular/core';
import { Employee, AddFormSubmit } from '../data/employeedata';
import { ActivatedRoute, Router } from '@angular/router';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';

@Component({
  selector: 'app-list-edit',
  templateUrl: './list-edit.component.html',
  styleUrls: ['./list-edit.component.css']
})
export class ListEditComponent implements OnInit {
  Keyword: string = '';
  AddFormSubmit: AddFormSubmit = new AddFormSubmit();
  EditFS: Employee = new Employee();
  EditId: number;

  constructor(private _employeeService: EmployeeService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.EditId = +this.route.snapshot.paramMap.get('id');
    this._employeeService.post<any>('/api/Employee/EditGet', this.EditId).subscribe(
      (data: Employee) => {
        this.EditFS = data;
      });
  }

  public Edit() {
    if (confirm('是否修改資料')) {
      this._employeeService.post<any>('/api/Employee/Edit', this.EditFS).subscribe(
        (data: Employee) => {
          this.EditFS = data;
        });
    }
  }
}
