import { Component, OnInit } from '@angular/core';
import { ActivityInfo, ActivityItem } from '../data/activitydata';
import { ActivatedRoute, Router } from '@angular/router';
// Import EmployeeService
import { EmployeeService } from '../service/employee.service';
import { LoginDate } from '../data/logindata';
import { ActivityUser } from '../data/activitydata';
import { Employee } from '../data/employeedata';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-activity-add',
  templateUrl: './activity-add.component.html',
  styleUrls: ['./activity-add.component.css']
})
export class ActivityAddComponent implements OnInit {

  LoginDate = new LoginDate();
  Item: ActivityItem[] = [];
  ActivityUser: ActivityUser = new ActivityUser();
  ActivityInfo: ActivityInfo = new ActivityInfo();
  UserList: Employee[] = [];
  msg: string = '';

  constructor(private _employeeService: EmployeeService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.LoginDate = this._employeeService.islogin();

    this._employeeService.post<any>('/api/Employee/Get', null).subscribe(
      (data: Employee[]) => {
        this.UserList = data;
      });

    this._employeeService.post<any>('/api/Activity/GetItem', null).subscribe(
      (data: ActivityItem[]) => {
        this.Item = data;
      });
  }

  Add() {
    if (!this.ActivityUser.UserId) {
      alert('需要選擇會員');
      return;
    }

    if (!this.ActivityUser.ActivityId) {
      alert('需要選擇活動');
      return;
    }

    if (confirm('是否新增報名')) {
      this._employeeService.post<any>('/api/Activity/Add', this.ActivityUser).subscribe(
        (data: string) => {
          alert(data);
        });
    }
  }
}
