
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ListEmployeesComponent } from './list-employees/list-employees.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
// Import RouterModule
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeService } from './service/employee.service';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { VerifyCodeComponent } from './verify-code/verify-code.component';
import { ListAddComponent } from './list-add/list-add.component';
import { ListEditComponent } from './list-edit/list-edit.component';
import { CookieService } from 'ngx-cookie-service';
import { AdminLogRecordComponent } from './admin-log-record/admin-log-record.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { ActivityAddComponent } from './activity-add/activity-add.component';

// Each route maps a URL path to a component
// The 3rd route specifies the route to redirect to if the path
// is empty. In our case we are redirecting to /list

// pathMatch property value can be full or prefix. For now we
// will set it to full as we want to do a full match
const appRoutes: Routes = [
  { path: 'list', component: ListEmployeesComponent },
  { path: 'create', component: CreateEmployeeComponent },
  { path: 'add', component: ListAddComponent },
  { path: 'edit/:id', component: ListEditComponent },
  { path: 'login', component: LoginComponent },
  { path: 'adminLogRecord', component: AdminLogRecordComponent },
  { path: 'activityList', component: ActivityListComponent },
  { path: 'activityadd', component: ActivityAddComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

// To let the router know about the routes configured above,
// pass "appRoutes" constant to forRoot(appRoutes) method
@NgModule({
  declarations: [
    AppComponent,
    ListEmployeesComponent,
    CreateEmployeeComponent,
    LoginComponent,
    VerifyCodeComponent,
    ListAddComponent,
    ListEditComponent,
    AdminLogRecordComponent,
    ActivityListComponent,
    ActivityAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [
    EmployeeService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
