import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from '../service/employee.service';
import { LoginDate } from '../data/logindata';
import { VerifyCodeComponent } from '../verify-code/verify-code.component';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('verifyCode', { static: true }) verifyCode: VerifyCodeComponent; // 获取页面中的验证码组件

  appcomponent: AppComponent;
  _sessionId: string;
  LoginDate = new LoginDate();
  vcode;
  isVerifyCode;
  VerifyCodeMsg;

  constructor(private _employeeService: EmployeeService, private router: Router, private cookieService: CookieService) {

    if (!!this._employeeService.islogin().ID) {
      this.router.navigate(['/list']);
    }
  }

  ngOnInit() {
  }

  login() {
    this.check();
    if (this.isVerifyCode) {
      this.LoginDate.IsVerifyCode = this.isVerifyCode;
      this._employeeService.Adminlogin(this.LoginDate);
    }
  }

  check() {
    if (!!this.vcode) {
      this.isVerifyCode = true;
    } else {
      this.VerifyCodeMsg = '請輸入验证码';
    }

    if (!this.verifyCode.validate(this.vcode)) { // this.code.value为用户输入的验证码
      this.isVerifyCode = false;
      this.VerifyCodeMsg = '验证码不正确';
      return;
    }
  }
}
