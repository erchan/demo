namespace demoAPI.Models.EFModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Admin")]
    public partial class Admin
    {
        public int ID { get; set; }

        [Required]
        [StringLength(500)]
        public string Account { get; set; }

        [Required]
        [StringLength(500)]
        public string Password { get; set; }
    }
}
