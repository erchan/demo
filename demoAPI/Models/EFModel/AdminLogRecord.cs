namespace demoAPI.Models.EFModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdminLogRecord")]
    public partial class AdminLogRecord
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public bool IsSuccess { get; set; }

        public int AdminId { get; set; }
    }
}
