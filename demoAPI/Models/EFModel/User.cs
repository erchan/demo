namespace demoAPI.Models.EFModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(500)]
        public string LastName { get; set; }

        [Required]
        [StringLength(500)]
        public string FirstName { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
