﻿using demoAPI.Models.EFModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace OktaAspNetMvcAngular.Models
{
    public class Login_Info
    {
        public string Msg { get; set; } = string.Empty;
        public string MsgClass { get; set; } = string.Empty;

        public class LoginDate
        {
            public int Id { get; set; } = 0;
            public string Account { get; set; } = string.Empty;
            public string Password { get; set; } = string.Empty;
            public bool IsVerifyCode { get; set; } = false;
        }

        public class AdminLogDate
        {
            public int Id { get; set; }
            public int AdminId { get; set; }
            public string Admin { get; set; }
            public DateTime CreatedAt { get; set; }
            public bool IsSuccess { get; set; }
        }

        //登入 log
        public List<AdminLogDate> LogGet()
        {
            List<AdminLogDate> adminLogList = new List<AdminLogDate>();

            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        adminLogList = db.AdminLogRecord.Select(a => new AdminLogDate()
                        {
                            Id = a.Id,
                            AdminId = a.AdminId,
                            Admin = db.Admin.Where(b => b.ID == a.AdminId).Select(b => b.Account).FirstOrDefault(),
                            IsSuccess = a.IsSuccess,
                            CreatedAt = a.CreatedAt

                        }).OrderByDescending(a => a.CreatedAt).ToList();
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return adminLogList;
        }

        //登入
        public Admin Login(LoginDate FS)
        {
            Admin admin = new Admin();

            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        var IsSuccess = false;

                        IQueryable<User> row = db.User;

                        var Single = db.Database.SqlQuery<Admin>("Admin_Login @account,@password", new SqlParameter("@account", FS.Account), new SqlParameter("@password", FS.Password));

                        if (Single.Count() > 0)
                        {
                            admin = db.Database.SqlQuery<Admin>("Admin_Login @account,@password", new SqlParameter("@account", FS.Account), new SqlParameter("@password", FS.Password)).Single();

                            if (!string.IsNullOrEmpty(admin.Account) && !string.IsNullOrEmpty(admin.Password))
                            {
                                IsSuccess = true;
                            }
                        }

                        var add_row = new AdminLogRecord()
                        {
                            AdminId = admin.ID,
                            IsSuccess = IsSuccess,
                            CreatedAt = DateTime.Now
                        };

                        db.AdminLogRecord.Add(add_row);
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return admin;
        }

        //登出
        public bool Logout()
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return true;
        }
    }
}
