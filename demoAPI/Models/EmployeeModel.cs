﻿using demoAPI.Models.EFModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace OktaAspNetMvcAngular.Models
{
    public class Employee_Info
    {
        public string Msg { get; set; } = string.Empty;
        public string MsgClass { get; set; } = string.Empty;
        public List<Employee> EmployeeList { set; get; } = new List<Employee>();

        public class Employee
        {
            public int Id { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public DateTime CreatedAt { get; set; }
        }

        public Employee GetEmployeeId(int Id)
        {
            Employee row = new Employee();
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        row = db.User.Where(u => u.Id == Id).Select(a => new Employee()
                        {
                            Id = a.Id,
                            LastName = a.LastName,
                            FirstName = a.FirstName,
                            CreatedAt = a.CreatedAt
                        }).OrderByDescending(a => a.CreatedAt).FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return row;
        }

        //撈資料
        public List<Employee> GetEmployee(string Keyword)
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        IQueryable<User> row = db.User;

                        if (!string.IsNullOrEmpty(Keyword))
                        {
                            row = row.Where(a => a.LastName.Contains(Keyword) && a.FirstName.Contains(Keyword));
                        }

                        EmployeeList = row.Select(a => new Employee()
                        {
                            Id = a.Id,
                            LastName = a.LastName,
                            FirstName = a.FirstName,
                            CreatedAt = a.CreatedAt
                        }).OrderByDescending(a => a.CreatedAt).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return EmployeeList;
        }

        //新增
        public void AddEmployee(AddFormSubmit FS)
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        IQueryable<User> row = db.User;

                        var add_row = new User()
                        {
                            LastName = FS.LastName,
                            FirstName = FS.FirstName,
                            CreatedAt = DateTime.Now
                        };

                        db.User.Add(add_row);
                        db.SaveChanges();

                        //MailMessage Message = new MailMessage();
                        //Message.From = new MailAddress("nahcreuw@gmail.com");
                        //Message.To.Add(new MailAddress("nahcreuw@gmail.com"));
                        //Message.Subject = "barneybarney";
                        //Message.Body = "barneybarneybarney";

                        //using (SmtpClient mysmtp = new SmtpClient("smtp.gmail.com", 587))
                        //{
                        //    mysmtp.Credentials = new NetworkCredential("erchan.wu@shinda.com.tw", "famxebwcadogfwud");
                        //    mysmtp.Host = "smtp.gmail.com";
                        //    mysmtp.EnableSsl = true;
                        //    mysmtp.Send(Message);
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                Msg = "新增失敗！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }
        }

        public class AddFormSubmit
        {
            public string LastName { get; set; }
            public string FirstName { get; set; }
            public DateTime CreatedAt { get; set; }
        }

        //編輯
        public void EditEmployee(Employee FS)
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        User row = db.User.Where(a => a.Id == FS.Id).First();

                        row.LastName = FS.LastName;
                        row.FirstName = FS.FirstName;
                        row.CreatedAt = FS.CreatedAt;
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "編輯失敗！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }
        }


        public Employee EditGet(int id)
        {
            Employee row = new Employee();

            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        row = db.User.Where(a => a.Id == id).Select(a => new Employee()
                        {
                            Id = a.Id,
                            LastName = a.LastName,
                            FirstName = a.FirstName,
                            CreatedAt = DateTime.Now
                        }).FirstOrDefault();
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return row;
        }

        //刪除
        public void DelEmployee(int Id)
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        User row = db.User.Where(a => a.Id == Id).First();

                        db.User.Remove(row);
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception e)
            {
                Msg = "刪除失敗！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }
        }
    }
}