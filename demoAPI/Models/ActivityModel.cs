﻿using demoAPI.Models.EFModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace OktaAspNetMvcAngular.Models
{
    public class Activity_Info
    {
        public string Msg { get; set; } = string.Empty;
        public string MsgClass { get; set; } = string.Empty;
        public List<ActivityInfo> ActivityList { set; get; } = new List<ActivityInfo>();

        public class ActivityInfo
        {
            public int Id { get; set; }
            public UserInfo User { get; set; }
            public DateTime CreatedAt { get; set; }
            public ActivityItem Activity { get; set; }
        }

        public class UserInfo
        {
            public int Id { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
        }

        public class ActivityItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class ActivityUser
        {
            public int UserId { get; set; }
            public int ActivityId { get; set; }
            public string Email { get; set; }
            public DateTime CreatedAt { get; set; }
        }

        public List<ActivityItem> GetItem()
        {
            List<ActivityItem> row = new List<ActivityItem>();

            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        IQueryable<Activity> rows = db.Activity;

                        row = rows.Select(a => new ActivityItem()
                        {
                            Id = a.Id,
                            Name = a.Name
                        }).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return row;
        }

        //撈報名資料
        public List<ActivityInfo> Get()
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        IQueryable<ActivityRegistration> row = db.ActivityRegistration;

                        ActivityList = row.Select(a => new ActivityInfo()
                        {
                            Id = a.Id,
                            User = db.User.Where(u => u.Id == a.UserId).Select(u => new UserInfo()
                            {
                                Id = u.Id,
                                LastName = u.LastName,
                                FirstName = u.FirstName,
                            }).FirstOrDefault(),
                            Activity = db.Activity.Where(i => i.Id == a.ActivityId ).Select(i => new ActivityItem()
                            {
                                Id = i.Id,
                                Name = i.Name
                            }).FirstOrDefault(),
                            CreatedAt = a.CreatedAt
                        }).OrderByDescending(a => a.CreatedAt).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                Msg = "伺服器錯誤！";
                MsgClass = "alert-danger";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return ActivityList;
        }

        //新增報名資料
        public string Add(ActivityUser FS)
        {
            try
            {
                using (var db = new DBModel())
                {
                    if (db.Database.Exists())
                    {
                        IQueryable<User> row = db.User;

                        var add_row = new ActivityRegistration()
                        {
                            UserId = FS.UserId,
                            ActivityId = FS.ActivityId,
                            CreatedAt = DateTime.Now
                        };

                        db.ActivityRegistration.Add(add_row);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                MsgClass = "alert-danger";
                return "新增失敗！";
            }
            if (string.IsNullOrWhiteSpace(Msg))
            {
                MsgClass = "hidden";
            }

            return "新增成功！";
        }
    }
}