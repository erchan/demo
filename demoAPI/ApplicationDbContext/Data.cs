﻿using System.Data.Entity;
using OktaAspNetMvcAngular.Models;

namespace OktaAspNetMvcAngular.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DBModel")
        {
        }

        public static ApplicationDbContext Create() => new ApplicationDbContext();
        
    }
}