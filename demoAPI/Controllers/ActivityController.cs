﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using demoAPI.Models.EFModel;
using OktaAspNetMvcAngular.Models;
using static OktaAspNetMvcAngular.Models.Activity_Info;
using static OktaAspNetMvcAngular.Models.Login_Info;

namespace demoAPI.Controllers
{
    public class ActivityController : ApiController
    {
        private Activity_Info Service;

        public ActivityController()
        {
            Service = new Activity_Info();
        }

        [HttpPost]
        public List<ActivityItem> GetItem()
        {
            List<ActivityItem> Info = Service.GetItem();

            return Info;
        }

        [HttpPost]
        public List<ActivityInfo> Get()
        {
            List<ActivityInfo> Info = Service.Get();

            return Info;
        }

        [HttpPost]
        public string Add(ActivityUser FS)
        {
            return Service.Add(FS);
        }
    }
}


