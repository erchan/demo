﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OktaAspNetMvcAngular.Models;
using static OktaAspNetMvcAngular.Models.Employee_Info;

namespace demoAPI.Controllers
{
    public class EmployeeController : ApiController
    {

        private Employee_Info Service;

        public EmployeeController()
        {
            Service = new Employee_Info();
        }

        [HttpPost]
        public Employee GetId([FromBody]int Id)
        {
            Employee Info = Service.GetEmployeeId(Id);

            return Info;
        }

        [HttpPost]
        public List<Employee> Get([FromBody]string Keyword)
        {
            List<Employee> Info = Service.GetEmployee(Keyword);

            return Info;
        }

        [HttpPost]
        public bool Add(AddFormSubmit FS)
        {
            Service.AddEmployee(FS);

            return true;
        }

        [HttpPost]
        public Employee Edit(Employee FS)
        {
            Service.EditEmployee(FS);

            return EditGet(FS.Id);
        }

        [HttpPost]
        public Employee EditGet([FromBody]int id)
        {
            Employee Info = Service.EditGet(id);

            return Info;
        }

        [HttpPost]
        public List<Employee> Del([FromBody]int Id)
        {
            Service.DelEmployee(Id);

            return Get("");
        }
    }
}


