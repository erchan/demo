﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using demoAPI.Models.EFModel;
using OktaAspNetMvcAngular.Models;
using static OktaAspNetMvcAngular.Models.Login_Info;

namespace demoAPI.Controllers
{
    public class LoginController : ApiController
    {
        private Login_Info Service;

        public LoginController()
        {
            Service = new Login_Info();
        }

        [HttpPost]
        public List<AdminLogDate> LogGet()
        {
            List<AdminLogDate> admin = Service.LogGet();

            return admin;
        }

        [HttpPost]
        public Admin Login(LoginDate FS)
        {
            Admin admin =   Service.Login(FS);
            
            return admin;
        }

        [HttpPost]
        public bool Logout()
        {
            var a = Service.Logout();
            
            return true;
        }
    }
}


